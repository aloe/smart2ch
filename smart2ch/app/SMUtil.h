//
//  SMUtil.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VknUtils.h"

#define APPLIPROMOTION_ICON_SHOW @"AppliPromotionIconShow"
#define APPLIPROMOTION_ICON_HIDE @"AppliPromotionIconHide"

@interface SMUtil : NSObject

+(UIViewController*)rootViewController;
+(void) setup:(UIViewController*)rootViewController_;
+(BOOL) isJaDevice;
+(void) tryShowRecommend;

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name;
+(void) dispatchGlobalEvent:(NSString*)name;
+(int) appActiveCount;
+(BOOL) isShowPopad;

@end
