//
//  Article.h
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModel.h"

@class Site;

@interface Article : VknModel

@property int siteId;
@property NSString* title;
@property NSString* content;
@property NSString* imageUrl;
@property NSString* linkUrl;
@property Site* site;
@property UIColor* bgColor;
@property UIColor* bgColor2;

+(Article*)createDummy;
-(id) initWithDictionary:(NSDictionary*)dic;

-(NSString*) dateStr;
-(BOOL)isFavorit;
-(BOOL) hasImage;
-(BOOL) isAd;
-(BOOL) isBase64Image;
-(NSString*) base64Image;

@end
