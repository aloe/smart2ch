//
//  ArticleList.h
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModelList.h"
#import "Article.h"

@interface ArticleList : VknModelList

+(ArticleList*)createDummy;

-(Article*)get:(int)index;
-(id) initWithJsonArray:(NSArray*)array;


@end
