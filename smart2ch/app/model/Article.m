//
//  Article.m
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "Article.h"
#import "Site.h"


@interface Article (){
    NSDate* date;
    BOOL favorit;
}

@end

@implementation Article

@synthesize title;
@synthesize siteId;
@synthesize content;
@synthesize imageUrl;
@synthesize linkUrl;
@synthesize site;
@synthesize bgColor;
@synthesize bgColor2;

+(Article*)createDummy{
    Article* article = [[Article alloc] init];
    
    return article;
}

-(id) initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if(self){
        id_ = [[dic objectForKey:@"id"] intValue];
        siteId = [[dic objectForKey:@"site_id"] intValue];
        title = [dic objectForKey:@"title"];
        content = [dic objectForKey:@"description"];
        imageUrl = [dic objectForKey:@"image_url"];
        linkUrl = [dic objectForKey:@"link_url"];
        date = [NSDate dateWithTimeIntervalSince1970:[[dic objectForKey:@"published_at"] intValue]];
        
        bgColor = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
//        bgColor2 = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
        bgColor2 = [UIColor colorWithHue:0.0f saturation:0.2f brightness:0.8f alpha:1];
    }
    
    return self;
}

-(NSString*) dateStr{
    return [VknDateUtil dateToString:date format:@"yyyy/MM/dd HH:mm"];
}

-(BOOL)isFavorit{
    return favorit;
}

-(BOOL) hasImage{
    return ([imageUrl length] != 0);
}

-(BOOL)isAd{
    return NO;
}

-(BOOL) isBase64Image{
    return NO;
}

-(NSString*) base64Image{
    return @"";
}

@end
