//
//  Model.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Model.h"
#import "VknUrlLoadCommand.h"
#import "Api.h"

@interface Model (){
    SiteList* siteList;
    ArticleList* articleList;
}

@end

@implementation Model

+ (Model*)getInstance {
    static Model* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Model alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

static NSString* apiJsonKey = @"apiJsonKey";

-(void) saveApiJson:(NSString*)jsonStr{
    [VknThreadUtil threadBlock:^{
        NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:jsonStr forKey:apiJsonKey];
        [ud setBool:YES forKey:initializedKey];
        [ud synchronize];
    }];
}

-(NSString*)apiString{
    return [[NSUserDefaults standardUserDefaults] stringForKey:apiJsonKey];
}

-(void) createModels:(NSDictionary*)dic{
    NSArray* articleArray = [dic objectForKey:@"article"];
    articleList = [[ArticleList alloc] initWithJsonArray:articleArray];
    
    NSArray* siteArray = [dic objectForKey:@"site"];
    siteList = [[SiteList alloc] initWithJsonArray:siteArray];
    Site* topSite = [[Site alloc] init];
    topSite.name = @"トップ";
    [siteList add:topSite at:0];
    
    for(int i=0,max=[articleList count];i<max;i++){
        Article* article = [articleList get:i];
        [topSite addArticle:article];
        for(int j=0,jMax=[siteList count];j<jMax;j++){
            Site* site = [siteList get:j];
            if(article.siteId == site.id_){
                article.site = site;
                [site addArticle:article];
                break;
            }
        }
    }
}

#pragma mark -------- public -----------

static NSString* initializedKey = @"dataInitializedKey";
-(BOOL) isDataInitialized{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    return [ud boolForKey:initializedKey];
}

-(SiteList*)siteList{
    return siteList;
}

-(void) loadModel:(ModelCallback_t)callback fail:(ModelCallback_t)failCallback{
    [VknThreadUtil threadBlock:^{
        
        NSString* apiPath = @"api.json";
        if(![SMUtil isJaDevice]){
            apiPath = @"noJa.php";
        }
        
        NSString* apiUrl = [Api url:apiPath];
        
        NSLog(@"apiUrl:%@", apiUrl);
        
        VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:apiUrl];
        c.onComplete = ^(NSData* data){
            NSDictionary* dic = [VknDataUtil dataToJson:data];
            if(dic == nil) return;
            
            NSString* jsonString = [VknDataUtil dataToString:data];
            [self saveApiJson:jsonString];
            
            [self createModels:dic];
            
            [VknThreadUtil mainBlock:^{
                callback(articleList);
            }];
        };
        c.onProgress = ^(float progress){
            NSLog(@"progress:%f", progress);
        };
        c.onFail = ^(NSError* error){
            [VknThreadUtil mainBlock:^{
                failCallback();
            }];
        };
        [c execute:nil];
    }];
}

-(void) loadModelFromLocal:(ModelCallback_t)callback{
    NSLog(@"loadModelFromLocal");
    NSString* jsonStrng = [self apiString];
    NSDictionary* dic = [VknDataUtil strToJson:jsonStrng];
    
    [self createModels:dic];
    callback(articleList);
}

@end
