//
//  Site.h
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModel.h"

@class ArticleList;
@class Article;

@interface Site : VknModel

+(Site*)createDummy;

@property NSString* name;

-(id)initWithDictionary:(NSDictionary*)dic;
-(ArticleList*)articleList;
-(void) addArticle:(Article*)article;

@end
