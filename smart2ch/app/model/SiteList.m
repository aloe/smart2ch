//
//  SiteList.m
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "SiteList.h"

@implementation SiteList

+(SiteList*)createDummy{
    SiteList* siteList = [[SiteList alloc] init];
    for(int i=0;i<100;i++){
        Site* site = [Site createDummy];
        [siteList add:site];
    }
    
    return siteList;
}

-(Site*)get:(int)index{
    return (Site*)[super get:index];
}

-(Site*)getById:(int)index{
    return (Site*)[super getById:index];
}

-(id) initWithJsonArray:(NSArray*)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            Site* site = [[Site alloc] initWithDictionary:dic];
            [self add:site];
        }
    }
    
    return self;
}

@end
