//
//  SiteList.h
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknModelList.h"
#import "Site.h"

@interface SiteList : VknModelList

+(SiteList*)createDummy;
-(Site*)get:(int)index;
-(Site*)getById:(int)index;
-(id) initWithJsonArray:(NSArray*)array;

@end
