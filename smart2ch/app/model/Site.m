//
//  Site.m
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "Site.h"
#import "ArticleList.h"


@interface Site (){
    ArticleList* articleList;
}

@end

@implementation Site

@synthesize name;

+(Site*)createDummy{
    Site* site = [[Site alloc] init];
    site.name = @"キニ速";
    
    return site;
}

-(id)initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if(self){
        articleList = [[ArticleList alloc] init];
        id_ = [[dic objectForKey:@"id"] intValue];
        name = [dic objectForKey:@"name"];
    }
    
    return self;
}

-(id) init{
    self = [super init];
    if(self){
        articleList = [[ArticleList alloc] init];
    }
    
    return self;
}

-(ArticleList*)articleList{
    return articleList;
}

-(void) addArticle:(Article*)article{
    [articleList add:article];
}

@end
