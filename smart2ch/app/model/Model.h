//
//  Model.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SiteList.h"
#import "ArticleList.h"

typedef void (^ModelSiteListCallback_t)(SiteList* siteList_);
typedef void (^ModelCallback_t)();

@interface Model : NSObject

+(Model*)getInstance;
-(BOOL) isDataInitialized;
-(void) loadModel:(ModelCallback_t)callback fail:(ModelCallback_t)failCallback;
-(void) loadModelFromLocal:(ModelCallback_t)callback;
-(SiteList*)siteList;


@end
