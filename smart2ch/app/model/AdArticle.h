//
//  AdArticle.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Article.h"
#import "ChkRecordData.h"
#import "AMoAdSDK.h"

@interface AdArticle : Article

@property NSString* detailContent;
@property NSString* appId;

-(id) initWithChkRecordData:(ChkRecordData*)data;
-(id) initWithAPSDKAd:(APSDKAd*)apAd;


@end
