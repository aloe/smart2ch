//
//  AdArticle.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdArticle.h"

@interface AdArticle (){
    NSString* base64Image;
    BOOL isBase64Image;
}

@end

@implementation AdArticle

@synthesize title;
@synthesize siteId;
@synthesize content;
@synthesize imageUrl;
@synthesize linkUrl;
@synthesize site;
@synthesize bgColor;
@synthesize detailContent;
@synthesize appId;

-(id) initWithChkRecordData:(ChkRecordData*)data{
    self = [super init];
    if(self){        
        title = data.title;
        content = data.description;
        imageUrl = data.imageIcon;
        linkUrl = data.linkUrl;
        detailContent = data.detail;
        appId = data.appStoreId;
        bgColor = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
        isBase64Image = NO;
    }
    
    return self;
}

-(id) initWithAPSDKAd:(APSDKAd *)apAd{
    self = [super init];
    if(self){
        title = apAd.appName;
        content = apAd.appText;
        isBase64Image = [apAd.imageType isEqualToString:APSDK_LOW_LEVEL_API_AD_IMAGE_TYPE_BASE64_ENCODED_STRING];
        if(isBase64Image){
            base64Image = apAd.image;
        }else{
            imageUrl = apAd.image;
        }
        
    }
    
    return self;
}

-(BOOL) isAd{ return YES; }

-(BOOL) isBase64Image{
    return isBase64Image;
}

-(NSString*) base64Image{
    return base64Image;
}

@end
