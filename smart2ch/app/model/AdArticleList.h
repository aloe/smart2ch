//
//  AdArticleList.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleList.h"
#import "AdArticle.h"

@interface AdArticleList : ArticleList

-(id) initWithChkDatalist:(NSArray*)chkDataList;
-(id) initWithAppAdList:(NSArray*)appAdList;

-(AdArticle*)get:(int)index;
+(AdArticleList*)createDummy;

@end
