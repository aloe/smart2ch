//
//  ArticleList.m
//  jade
//
//  Created by kawase yu on 2014/07/25.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "ArticleList.h"

@implementation ArticleList

+(ArticleList*)createDummy{
    ArticleList* articleList = [[ArticleList alloc] init];
    for(int i=0;i<100;i++){
        Article* article = [Article createDummy];
        [articleList add:article];
    }
    
    return articleList;
}

-(Article*)get:(int)index{
    return (Article*)[super get:index];
}

-(id) initWithJsonArray:(NSArray*)array{
    self = [super init];
    if(self){
        for(NSDictionary* dic in array){
            Article* article = [[Article alloc] initWithDictionary:dic];
            [self add:article];
        }
    }
    
    return self;
}

@end
