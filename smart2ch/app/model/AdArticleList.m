//
//  AdArticleList.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdArticleList.h"
#import "AdArticle.h"

@implementation AdArticleList

-(id) initWithChkDatalist:(NSArray*)chkDataList{
    self = [super init];
    if(self){
        for(ChkRecordData* data in chkDataList){
            AdArticle* adArticle = [[AdArticle alloc] initWithChkRecordData:data];
            [self add:adArticle];
        }
    }
    
    return self;
}

-(id) initWithAppAdList:(NSArray*)appAdList{
    self = [super init];
    if(self){
        for(APSDKAd* obj in appAdList){
            AdArticle* article = [[AdArticle alloc] initWithAPSDKAd:obj];
            [self add:article];
        }
    }
    
    return self;
}

-(AdArticle*)get:(int)index{
    return (AdArticle*)[super get:index];
}

+(AdArticleList*)createDummy{
    AdArticleList* list = [[AdArticleList alloc] init];
    AdArticle* ad = [[AdArticle alloc] init];
    ad.title = @"薬剤師 年収処方箋";
    ad.content = @"わたしの年収って高い？安い？？人には聞けない・・・でも気になる！！そんな薬剤師さんのお悩みには「薬剤師年収処方箋」を！ ";
    ad.imageUrl = @"https://s3.mzstatic.com/us/r30/Purple5/v4/08/81/69/088169da-4eef-3f69-932e-7f3c90e7eb84/icon170x170.png";
    ad.linkUrl = @"https://itunes.apple.com/us/app/yao-ji-shi-nian-shou-zhen/id899049413?l=ja&ls=1&mt=8";
    ad.appId = @"899049413";
    ad.bgColor = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
    [list add:ad];
    
    ad = [[AdArticle alloc] init];
    ad.title = @"もう笑いました？～絶対吹くｗ爆笑スレ大量追加!";
    ad.content = @"「NEVERまとめ」より最高に笑えるスレを収録!!あなたはもう笑いましたか？ﾟ+｡(ﾉ´∀｀)ﾉ  ";
    ad.imageUrl = @"https://s5.mzstatic.com/us/r30/Purple/v4/d5/d6/98/d5d69893-14a5-6596-85d5-6a3da218e197/icon170x170.png";
    ad.linkUrl = @"https://itunes.apple.com/us/app/mou-xiaoimashita-jue-dui-chuikuw/id634891205?l=ja&ls=1&mt=8";
    ad.appId = @"634891205";
    ad.bgColor = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
    [list add:ad];
    
    ad = [[AdArticle alloc] init];
    ad.title = @"お買い物メモ ～手書き風の可愛いメモ帳～";
    ad.content = @"買い忘れがもう無くなる！？\n"
    "“お買い物メモ”を使えば、そんな手間や失敗がなくなるかも！？ ";
    ad.imageUrl = @"https://s2.mzstatic.com/us/r30/Purple3/v4/7e/78/8d/7e788d29-c3bc-e44e-b5d4-14a9eac918b8/icon170x170.png";
    ad.linkUrl = @"https://itunes.apple.com/us/app/o-maii-wumemo-shou-shuki-fengno/id607634035?l=ja&ls=1&mt=8";
    ad.appId = @"607634035";
    ad.bgColor = [UIColor colorWithHue:((float)arc4random_uniform(100)/100.0f) saturation:0.2f brightness:0.8f alpha:1];
    [list add:ad];
    
    return list;
}

@end
