//
//  Api.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/12.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Api.h"

@implementation Api

+(NSString*)url:(NSString*)path{
    return [NSString stringWithFormat:@"%@/%@", API_ENDPOINT, path];
}

@end
