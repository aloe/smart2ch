//
//  Page.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PageDelegate <NSObject>

-(void) onShowWebView;
-(void) onHideWebView;

@end

@interface Page : NSObject

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_ index:(int)index_;
-(UIView*)getView;
-(void) reload:(Site*)site;
-(void) reloadStatic:(Site *)site;
-(void) toRootViewcontroller;

@end
