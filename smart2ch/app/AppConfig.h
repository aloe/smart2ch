//
//  AppConfig.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/12.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

// アドステア
#define ADSTIR_MEDIA_ID @"MEDIA-13f602d8"
#define ADSTIR_SPOT_ID 14
#define ADSTIR_SPOT_ID_ARTICLE 13

// POPAD
#define POPAD_MEDIA_ID @"174"
#define POPAD_TAG_INITIAL @"initial"
#define POPAD_TAG_BACKTOLIST @"backToList"

// applipromotion
#define APPLIPROMOTION_APP_ID @"Y88Z1WZQEAEAAXK4"

// 右下のアイコン
#define AD_ICON_URL @"http://noraappli.com/dandan/tuber/icon/"

// api
#define API_ENDPOINT @"http://bakusoku.nowhappy.info/api"

// レビュー訴求で飛ぶとこ
#define APPSTORE_URL @"https://itunes.apple.com/us/app/id923822530?l=ja&ls=1&mt=8"

// アナリティクス
#define GA_TRACKCODE @"UA-55225208-1"

// 広告出す割あい
#define AD_RATE_IN_LIST 5

@interface AppConfig : NSObject

@end
