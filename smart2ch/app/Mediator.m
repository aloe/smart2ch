//
//  Mediator.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Mediator.h"
#import "Menu.h"
#import "RootViewController.h"
#import "Page.h"
#import "Ad.h"
#import "VknPostCommand.h"
#import "Api.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface Mediator ()<
MenuDelegate
, PageDelegate
, UIScrollViewDelegate
, UIAlertViewDelegate
, UIWebViewDelegate
>{
    UIWindow* window;
    Menu* menu;
    UIScrollView* mainSv;
    NSArray* pageList;
    int currntPage;
    UIImageView* splashView;
    RootViewController* rootViewController;
    UIView* todayGameIcon;
    BOOL isGameIconAnimating;
    UIButton* todayIcon;
}

@end

@implementation Mediator

-(id) initWithWindow:(UIWindow*)window_{
    self = [super init];
    if(self){
        window = window_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self setupView];
    [self initialLoad];
    
    [[Ad getInstance] setup:rootViewController];
    
    // analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 10;
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
    [[GAI sharedInstance] trackerWithTrackingId:GA_TRACKCODE];
    [self trackScreen:@"トップ"];
    
    // event
    [SMUtil addGlobalEventlistener:self selector:@selector(showAppliPromotionIcon) name:APPLIPROMOTION_ICON_SHOW];
    [SMUtil addGlobalEventlistener:self selector:@selector(hideAppliPromotionIcon) name:APPLIPROMOTION_ICON_HIDE];
}

-(void) showAppliPromotionIcon{
//    if(isGameIconAnimating) return;
//    isGameIconAnimating = YES;
    if(todayGameIcon.alpha == 1.0f) return;
    [UIView animateWithDuration:0.2f animations:^{
        todayIcon.transform = todayGameIcon.transform = CGAffineTransformMakeTranslation(0, 0);
        todayIcon.alpha = todayGameIcon.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        isGameIconAnimating = NO;
    }];
}

-(void) hideAppliPromotionIcon{
//    if(isGameIconAnimating) return;
//    isGameIconAnimating = YES;
    if(todayGameIcon.alpha == 0) return;
    [UIView animateWithDuration:0.2f animations:^{
        todayIcon.transform = todayGameIcon.transform = CGAffineTransformMakeTranslation(0, 20);
        todayIcon.alpha = todayGameIcon.alpha = 0;
    } completion:^(BOOL finished) {
        isGameIconAnimating = NO;
    }];
}

-(void) trackScreen:(NSString*)screenName{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void) setupView{
    menu = [[Menu alloc] initWithDelegate:self];
    
    rootViewController = [[RootViewController alloc] init];
    [SMUtil setup:rootViewController];
    
    UIView* rootView = rootViewController.view;
    [rootView addSubview:[menu getView]];
    
    float svHeight = [VknDeviceUtil windowHeight]-64;
    mainSv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, 320, svHeight)];
    mainSv.contentSize = CGSizeMake(320*3, svHeight);
    mainSv.pagingEnabled = YES;
    mainSv.delegate = self;
    mainSv.showsHorizontalScrollIndicator = NO;
    
    [rootView addSubview:mainSv];
    window.rootViewController = rootViewController;
    
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0;i<3;i++){
        Page* page = [[Page alloc] initWithDelegate:self index:i];
        [tmp addObject:page];
        [mainSv addSubview:[page getView]];
    }
    pageList = [NSArray arrayWithArray:tmp];
    
    // applipromotion
    
    todayGameIcon = [[UIView alloc] initWithFrame:CGRectMake(320-57-10, [VknDeviceUtil windowHeight]-57-10, 57, 57)];
    [todayGameIcon addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showApplipromotion)]];
    [rootView addSubview:todayGameIcon];
    
    UIWebView* todayGameIconWeb = [[UIWebView alloc] init];
    [todayGameIconWeb setBackgroundColor:[UIColor clearColor]];
    [todayGameIconWeb setOpaque:NO];
    todayGameIconWeb.frame = CGRectMake(0, 0, 57, 57);
    [todayGameIconWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:AD_ICON_URL]]];
    [todayGameIcon addSubview:todayGameIconWeb];
    todayGameIcon.hidden = ![SMUtil isJaDevice];
    
    todayIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    [todayIcon setImage:[UIImage imageNamed:@"todayGame"] forState:UIControlStateNormal];
    todayIcon.frame = CGRectMake(10, [VknDeviceUtil windowHeight]-70-4, 70, 70);
    todayIcon.hidden = ![SMUtil isJaDevice];
    [todayIcon addTarget:self action:@selector(showApplipromotion) forControlEvents:UIControlEventTouchUpInside];
    [rootView addSubview:todayIcon];
    
    if(![[Model getInstance] isDataInitialized]){
        splashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash-568h"]];
        [rootView addSubview:splashView];
        [rootViewController showLoading];
    }
}

-(void) showApplipromotion{
    [[Ad getInstance] showApplipromotion];
}

-(void) loadModel{
    Model* m = [Model getInstance];
//    [rootViewController showLoading];
    [m loadModel:^{
        [[Ad getInstance] reload];
        [menu reload];
        [self loadFirstView];
        [rootViewController hideLoading];
        if(splashView != nil){
            [UIView animateWithDuration:0.2f animations:^{
                splashView.alpha = 0;
            } completion:^(BOOL finished) {
                [splashView removeFromSuperview];
                splashView = nil;
            }];
        }
    } fail:^{
        if([[Model getInstance] isDataInitialized]) return;
        
        UIAlertView* alertView = [[UIAlertView alloc] init];
        alertView.title = @"通信に失敗しました";
        alertView.message = @"しばらくしてから再度お試しください";
        [alertView addButtonWithTitle:@"再試行"];
        alertView.delegate = self;
        [alertView show];
    }];
    
    if([[Model getInstance] isDataInitialized]){
        [SMUtil tryShowRecommend];
    }
}

-(void) initialLoad{
    if([[Model getInstance] isDataInitialized]){
        [[Model getInstance] loadModelFromLocal:^{
            [menu reload];
            [menu active:0];
            [self loadFirstView];
        }];
    }
    
    [self loadModel];
}


-(void) loadFirstView{
    
    int currntIndex = [menu currentIndex];
    int start;
    int end;
    if(currntIndex == 0){
        start = 0;
        end = 2;
    }else{
        start = currntIndex - 1;
        end = currntIndex + 1;
    }
    
    SiteList* siteList = [[Model getInstance] siteList];
    int pageIndex = 0;
    for(int i=start;i<=end;i++){
        Site* site = [siteList get:i];
        Page* page = [pageList objectAtIndex:pageIndex];
        [page reloadStatic:site];
        pageIndex++;
    }
}

-(void) changePage{
    NSLog(@"1");
    [menu active:currntPage];
    SiteList* siteList = [[Model getInstance] siteList];
    if(currntPage == 0){
        for(int i=0;i<3;i++){
            Site* site = [siteList get:i];
            Page* p = [pageList objectAtIndex:i];
            [p reload:site];
        }
        mainSv.contentOffset = CGPointMake(0, 0);
        return;
    }
    
    NSLog(@"2");
    
    int pageIndex = 0;
    int maxPage = [siteList count]-1;
    if(currntPage == maxPage){
        for(int i=maxPage-2;i<=maxPage;i++){
            Site* site = [siteList get:i];
            Page* p = [pageList objectAtIndex:pageIndex];
            [p reload:site];
            pageIndex++;
        }
        mainSv.contentOffset = CGPointMake(640, 0);
        return;
    }
    
    for(int i=currntPage-1;i<=currntPage+1;i++){
        Site* site = [siteList get:i];
        Page* p = [pageList objectAtIndex:pageIndex];
        [p reload:site];
        pageIndex++;
    }
    mainSv.contentOffset = CGPointMake(320, 0);
}

#pragma mark -------- MenuViewDelegate -----------

-(void) onSelectSite:(int)siteIndex{
    if(currntPage == siteIndex) return;
    currntPage = siteIndex;
    
    for(Page* page in pageList){
        [page toRootViewcontroller];
    }
    
    [self changePage];
}

#pragma mark -------- PageDelegate -----------

-(void) onShowWebView{
    mainSv.scrollEnabled = NO;
}

-(void) onHideWebView{
    mainSv.scrollEnabled = YES;
    [[Ad getInstance] showPopadBacktolist];
}

#pragma mark -------- UIScrollViewDelegate -----------

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int page = scrollView.contentOffset.x / 320;
    int nextPage;
    
    SiteList* siteList = [[Model getInstance] siteList];
    int maxPage = [siteList count]-1;
    
    if(page == 2 || (currntPage == 0 && page == 1)){
        nextPage = currntPage + 1;
    }else if(page == 0 || (currntPage == maxPage && page == 1)){
        nextPage = currntPage-1;
    }else{
        return;
    }
    
    if(nextPage < 0) nextPage = 0;
    if(nextPage > maxPage) nextPage = maxPage;
    
    if(nextPage == currntPage) return;
    currntPage = nextPage;
    
    [self changePage];
}

#pragma mark -------- UIAlertViewDelegate -----------

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self loadModel];
}

#pragma mark -------- UIApplicationDelegate -----------

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [self loadModel];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

-(void) applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [VknImageCache clear];
}

-(void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* deviceTokenStr = [VknDataUtil deviceTokenToStr:deviceToken];
    NSLog(@"deviceTokenStr:%@", deviceTokenStr);
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSDictionary* param = @{
                            @"devicetoken": deviceTokenStr
                            , @"version": version
                            };
    
    NSString* registerUrl = [Api url:@"registerToken.php"];
    VknPostCommand* c = [[VknPostCommand alloc] initWithValues:param withUrl:registerUrl];
    c.onComplete = ^(NSData* data){
        NSLog(@"%@", [VknDataUtil dataToString:data]);
    };
    c.onFail = ^(NSError* error){
        NSLog(@"fail:%@", error.debugDescription);
    };
    [c execute:nil];
}

//#pragma mark -------- UIWebViewDelegate -----------
//
//-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
//    if(navigationType == UIWebViewNavigationTypeLinkClicked){
//        return NO;
//    }
//    
//    return YES;
//}

@end
