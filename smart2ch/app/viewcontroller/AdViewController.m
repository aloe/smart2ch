//
//  AdViewController.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdViewController.h"
#import "VknURLLoadCommand.h"

@interface AdViewController (){
    __weak NSObject<AdViewDelegate>* delegate;
    AdArticle* adArticle;
    
    UILabel* titleLabel;
    UILabel* detailLabel;
    UILabel* descriptionLabel;
    UILabel* ratingLabel;
    UIImageView* imageView;
    UIView* starOnMask;
    UIView* starOnView;
    UIScrollView* sv;
    
    UIScrollView* screenshotSv;
}

@end

@implementation AdViewController

-(id) initWithDelegate:(NSObject<AdViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.view.backgroundColor = [UIColor whiteColor];
    [self setContents];
    
    // event
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tapBack)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];

}

-(void) setContents{
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64)];
    sv.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    [self.view addSubview:sv];
    
    imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(12, 12, 85, 85);
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 20;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    [sv addSubview:imageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(107, 12, 190, 65)];
    titleLabel.font = [UIFont systemFontOfSize:16.0f];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.numberOfLines = 3;
    [sv addSubview:titleLabel];
    
//    84 × 16
    UIView* starView = [[UIView alloc] initWithFrame:CGRectMake(111, 88, 42, 8)];
    [sv addSubview:starView];
    UIImageView* starOff = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"starOff"]];
    [starView addSubview:starOff];
    UIImageView* starOn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"starOn"]];
    starOnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 8)];
    [starOnView addSubview:starOn];
    starOnView.layer.masksToBounds = YES;
    [starView addSubview:starOnView];
    
    starOnMask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 42, 8)];
    starOnMask.transform = CGAffineTransformMakeScale(0, 1.0);
    starOnMask.clipsToBounds = YES;
    starOnView.maskView = starOnMask;
    [starOnView addSubview:starOnMask];
    
    ratingLabel = [[UILabel alloc] init];
    ratingLabel.font = [UIFont systemFontOfSize:10.0f];
    ratingLabel.textColor = UIColorFromHex(0x909090);
    ratingLabel.frame = CGRectMake(155, 81, 30, 20);
    [sv addSubview:ratingLabel];
    
    UIButton* downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [downloadButton setImage:[UIImage imageNamed:@"downloadButton"] forState:UIControlStateNormal];
    [downloadButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
    downloadButton.frame = CGRectMake(230, 75, 78.5f, 23);
    [sv addSubview:downloadButton];
    
    detailLabel = [[UILabel alloc] init];
    detailLabel.numberOfLines = 0;
    [sv addSubview:detailLabel];
    
    descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.numberOfLines = 0;
    [sv addSubview:descriptionLabel];
    
    screenshotSv = [[UIScrollView alloc] init];
    [sv addSubview:screenshotSv];
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(320-55, [VknDeviceUtil windowHeight]-64-55, 45, 45);
    [backButton setImage:[UIImage imageNamed:@"adBackButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

-(void) tapButton{
    [VknUtils openBrowser:adArticle.linkUrl];
}

-(void) tapBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) reload:(AdArticle*)adArticle_{
    adArticle = adArticle_;
    
    titleLabel.text = adArticle.title;
    [titleLabel sizeToFit];
    CGRect titleFrame = titleLabel.frame;
    titleFrame.origin.x = 107;
    titleFrame.origin.y = 12;
    titleLabel.frame = titleFrame;
    
    float y = 108;
    
    screenshotSv.frame = CGRectMake(0, y, 320, 372);
    screenshotSv.backgroundColor = UIColorFromHex(0xeeeeee);
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(-100, 0, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    line = [[UIView alloc] initWithFrame:CGRectMake(-100, 371.5f, 1000, 0.5f)];
    line.backgroundColor = UIColorFromHex(0xcccccc);
    [screenshotSv addSubview:line];
    
    y += 372 + 15;
    
    descriptionLabel.frame = CGRectMake(10, y, 300, 10000);
    descriptionLabel.text = adArticle.content;
    [descriptionLabel sizeToFit];
    y += descriptionLabel.frame.size.height + 15;
    
    detailLabel.frame = CGRectMake(10, y, 300, 10000);
    detailLabel.text = adArticle.detailContent;
    [detailLabel sizeToFit];
    y += detailLabel.frame.size.height + 15;
    
    NSMutableArray *imageList = [NSMutableArray array];
    [imageList addObject:[UIImage imageNamed:@"bt1"]];
    [imageList addObject:[UIImage imageNamed:@"bt2"]];
//    460 136
    UIImageView* downloadButton = [[UIImageView alloc] initWithFrame:CGRectMake(45, y, 230, 68)];
    downloadButton.userInteractionEnabled = YES;
    [downloadButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapButton)]];
    [sv addSubview:downloadButton];
    y += downloadButton.frame.size.height + 15;
    downloadButton.animationImages = imageList;
    downloadButton.animationDuration = 1.5;
    downloadButton.animationRepeatCount = 0;
    [downloadButton startAnimating];
    
    sv.contentSize = CGSizeMake(320, y);
    
    [VknImageCache loadImage:adArticle.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:adArticle.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            imageView.image = image;
        }];
    }];
    
    [self loadApi];
}

-(void) loadApi{
    NSString* api = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?country=JP&id=%@", adArticle.appId];
    NSLog(@"%@", api);
    VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:api];
    c.onComplete = ^(NSData* data){
        NSDictionary* jsonDic = [VknDataUtil dataToJson:data];
        if(jsonDic == nil) return;
        NSArray* results = [jsonDic objectForKey:@"results"];
        if(results == nil) return;
        if([results count] == 0) return;
        NSDictionary* result = [results firstObject];
        NSArray* urls = [result objectForKey:@"screenshotUrls"];
        float rating = [[result objectForKey:@"averageUserRatingForCurrentVersion"] floatValue];
        int ratingCount = [[result objectForKey:@"userRatingCountForCurrentVersion"] intValue];
        NSString* version = [result objectForKey:@"version"];
        
        float per = rating / 5.0f;
        starOnView.frame = CGRectMake(0, 0, 42.0f*per, 8);
        ratingLabel.text = [NSString stringWithFormat:@"(%.1f)", rating];
        for(int i=0,max=[urls count];i<max;i++){
            NSString* url = [urls objectAtIndex:i];
            [self loadScreenshot:url index:i];
        }
        
        screenshotSv.contentSize = CGSizeMake(([urls count] * 210) + 10, 372);
    };
    
    [c execute:nil];
}

-(void) loadScreenshot:(NSString*)url index:(int)index{
    CGRect frame = CGRectMake(index * 210+10, 10, 200, 352);
    UIImageView* ssView = [[UIImageView alloc] initWithFrame:frame];
    ssView.contentMode = UIViewContentModeScaleAspectFit;
    [screenshotSv addSubview:ssView];
    screenshotSv.showsHorizontalScrollIndicator = NO;
    [VknImageCache loadImage:url defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        [VknThreadUtil mainBlock:^{
            ssView.image = image;
        }];
    }];
}


@end
