//
//  ArticleListViewController.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleListViewController.h"
#import "ArticleCell.h"
#import "AdstirView.h"
#import "AdArticleCell.h"
#import "AdgameCell.h"
#import "Ad.h"

@interface ArticleListViewController ()<
UITableViewDataSource
, UITableViewDelegate
, UIScrollViewDelegate
, ArticleCellDelegate
, AdArticleDelegate
, AdgameCellDelegate
>{
    __weak NSObject<ArticleListViewDelegate>* delegate;
    UITableView* tableView_;
    ArticleList* articleList;
    AdgameCell* adgameCell;
    float currntY;
}

@end

@implementation ArticleListViewController


-(id) initWithDelegate:(NSObject<ArticleListViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64)];
    tableView_.delegate = self;
    tableView_.dataSource = self;
    tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView_.backgroundColor = UIColorFromHex(0xffffff);
    tableView_.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
    [self.view addSubview:tableView_];
    
    adgameCell = [[AdgameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CdGameCell"];
    [adgameCell setDelegate:self];
}

-(void) ad{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 255)];
    AdstirView* adView = [[AdstirView alloc] initWithFrame:CGRectMake(0, 5, 300, 250)];
    [view addSubview:adView];
    adView.spot = ADSTIR_SPOT_ID;
    adView.media = ADSTIR_MEDIA_ID;
    [adView start];
    tableView_.tableFooterView = view;
}

-(void) reload:(Site*)site{
    [self reloadStatic:site];
    tableView_.contentOffset = CGPointMake(0, 0);
}

-(void) reloadStatic:(Site*)site{
    articleList = [site articleList];
    NSLog(@"%@, %d", site.name, [articleList count]);
    [tableView_ reloadData];
    
}

-(BOOL) isAdRow:(NSIndexPath*)indexPath{
    if(![SMUtil isJaDevice]){
        return NO;
    }
    int row = indexPath.row;
    return (row %AD_RATE_IN_LIST == 0 && row != 0);
}

#pragma mark -------- ArticleCellDelegate -----------

-(void) onSelectArticle:(Article *)article{
    [delegate onSelectArticle:article];
}

#pragma mark -------- AdArticleCellDelegate -----------

-(void) onSelectAdArticle:(AdArticle *)adArticle{
    [delegate onSelectAdArticle:adArticle];
}

#pragma mark -------- AdGamecellDelegate -----------

-(void) onSelectAdgame{
    [[Ad getInstance] showApplipromotion];
}

#pragma mark -------- UIWebViewDelegate -----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float move = y - currntY;
    float sa = scrollView.contentSize.height - scrollView.frame.size.height - y;
    if(sa < 0) return;
    
    if(move > 0 && y > 0){
        // hide
         [SMUtil dispatchGlobalEvent:APPLIPROMOTION_ICON_HIDE];
    }
    
    if(move < 0){
        //show
         [SMUtil dispatchGlobalEvent:APPLIPROMOTION_ICON_SHOW];
    }
    
    currntY = y;
}

#pragma mark -------- UITableViewDelegate -----------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self isAdRow:indexPath]){
        return 100.0f;
    }
    
    if([self isMiniRow:indexPath]){
        return 100.0f;
    }
    return 160.0f;
}

-(BOOL) isMiniRow:(NSIndexPath*)indexPath{
    int currnt = indexPath.row * 2;
    return (![[articleList get:currnt] hasImage] && ![[articleList get:currnt+1] hasImage]);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [articleList count] / 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier = @"Cell";
    NSString *adCellIdentifier = @"AdCell";
    if([self isAdRow:indexPath]){
        AdArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:adCellIdentifier];
        if (!cell) {
            cell = [[AdArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:adCellIdentifier];
            [cell setDelegate:self];
        }
        [cell reload:[[Ad getInstance] adArticle] index:indexPath.row];
        
        return cell;
    }
    
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[ArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setDelegate:self];
    }
    
    int currnt = indexPath.row * 2;
    Article* leftArticle = [articleList get:currnt];
    Article* rightArticle = [articleList get:currnt+1];
    [cell reload:leftArticle rightArticle:rightArticle index:indexPath.row];
    
    if([self isMiniRow:indexPath]){
        [cell mini];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    Article* article = [articleList get:indexPath.row];
//    [delegate onSelectArticle:article];
}



@end
