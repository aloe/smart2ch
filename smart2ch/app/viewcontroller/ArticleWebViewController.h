//
//  ArticleWebViewController.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "BaseViewController.h"
#import "Article.h"

@protocol ArticleWebViewDelegate <NSObject>

@end

@interface ArticleWebViewController : BaseViewController

-(id) initWithDelegate:(NSObject<ArticleWebViewDelegate>*)delegate_;
-(void) reload:(Article*)article_;

@end
