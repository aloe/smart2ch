//
//  ArticleWebViewController.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleWebViewController.h"
#import "VknUrlLoadCommand.h"
#import <Social/Social.h>
#import "Api.h"
#import "AdstirView.h"

@interface ArticleWebViewController ()<
UIWebViewDelegate
, UIScrollViewDelegate
, UIActionSheetDelegate
>{
    __weak NSObject<ArticleWebViewDelegate>* delegate;
    Article* article;
    UIView* header;
    UIView* menuView;
    UILabel* titleLabel;
    UIWebView* webView_;
    float currntY;
    BOOL firstLoad;
    AdstirView* adView;
    
    UIButton* webBackButton;
    UIButton* webNextButton;
}

@end

@implementation ArticleWebViewController

-(id) initWithDelegate:(NSObject<ArticleWebViewDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    [self setWebView];
    [self setHeader];
    [self setAdView];
    [self setMenu];
    
    self.view.backgroundColor = UIColorFromHex(0xcccccc);
    UISwipeGestureRecognizer* reco = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    reco.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:reco];
}

-(void) setWebView{
    webView_ = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64)];
    webView_.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0);
    webView_.delegate = self;
    webView_.scrollView.delegate = self;
    [self.view addSubview:webView_];
}

-(void) setHeader{
//    header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
//    UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
//    bgView.backgroundColor = UIColorFromHex(0xcccccc);
//    [header addSubview:bgView];
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, 65, 40);
    [header addSubview:backButton];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 0, 320-75-5, 40)];
    titleLabel.font = [UIFont systemFontOfSize:12.0f];
    titleLabel.textColor = UIColorFromHex(0x555555);
    titleLabel.numberOfLines = 2;
    [header addSubview:titleLabel];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(65, 5, 1, 30)];
    line.backgroundColor = [UIColor whiteColor];
    [header addSubview:line];
    
    [self.view addSubview:header];
}

-(void) setAdView{
    adView = [[AdstirView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-64-50-50, 320, 50)];
    [self.view addSubview:adView];
    adView.spot = ADSTIR_SPOT_ID_ARTICLE;
    adView.media = ADSTIR_MEDIA_ID;
    [adView start];
}

-(void) setMenu{
    menuView = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-64-50, 320, 50)];
    UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    bgView.backgroundColor = UIColorFromHex(0xf7f7f6);
    [menuView addSubview:bgView];
    [self.view addSubview:menuView];
    
    CGRect buttonFrame = CGRectMake(0, 5, 64, 40);
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = buttonFrame;
    [backButton setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(tapBack) forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:backButton];
    buttonFrame.origin.x += 64;
    
    webBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    webBackButton.frame = buttonFrame;
    [webBackButton setImage:[UIImage imageNamed:@"webBackButton"] forState:UIControlStateNormal];
    [webBackButton addTarget:self action:@selector(tapWebBack) forControlEvents:UIControlEventTouchUpInside];
    webBackButton.enabled = NO;
    [menuView addSubview:webBackButton];
    buttonFrame.origin.x += 64;
    
    webNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    webNextButton.frame = buttonFrame;
    [webNextButton setImage:[UIImage imageNamed:@"webNextButton"] forState:UIControlStateNormal];
    [webNextButton addTarget:self action:@selector(tapWebNext) forControlEvents:UIControlEventTouchUpInside];
    webNextButton.enabled = NO;
    [menuView addSubview:webNextButton];
    buttonFrame.origin.x += 64;
    
    UIButton* reloadButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    reloadButotn.frame = buttonFrame;
    [reloadButotn setImage:[UIImage imageNamed:@"webReloadButton"] forState:UIControlStateNormal];
    [reloadButotn addTarget:self action:@selector(tapWebReload) forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:reloadButotn];
    buttonFrame.origin.x += 64;
    
    
    UIButton* shareButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButotn.frame = buttonFrame;
    [shareButotn setImage:[UIImage imageNamed:@"webShareButton"] forState:UIControlStateNormal];
    [shareButotn addTarget:self action:@selector(tapWebShare) forControlEvents:UIControlEventTouchUpInside];
    [menuView addSubview:shareButotn];
    buttonFrame.origin.x += 64;
    
    [self.view addSubview:menuView];
}

-(void) tapWebBack{
    if([webView_ canGoBack]){
        [self showLoading];
        [webView_ goBack];
    }
}

-(void) tapWebNext{
    if([webView_ canGoForward]){
        [self showLoading];
        [webView_ goForward];
    }
}

-(void) tapWebReload{
    [self showLoading];
    [webView_ reload];
}

-(void) tapWebShare{
    NSString* title = [webView_ stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString* urlString = [webView_ stringByEvaluatingJavaScriptFromString:@"document.URL"];
    
//    UIImage* image = [UIImage imageNamed:@"icon_120"];
//    NSURL* url   = [NSURL URLWithString:urlString];
//    NSArray* activityItems = @[title, url, image];
//    
//    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:activityItems
//                                                                               applicationActivities:@[]];
//    [[SMUtil rootViewController] presentViewController:activityView animated:YES completion:nil];
    
    // アクションシートの作成
    
    UIActionSheet* actionSheet = [[UIActionSheet alloc] init];
    actionSheet.delegate = self;
    actionSheet.title = [NSString stringWithFormat:@"%@\n%@", title, urlString];
    [actionSheet addButtonWithTitle:@"Twitter"];
    [actionSheet addButtonWithTitle:@"Facebook"];
    [actionSheet addButtonWithTitle:@"LINE"];
    [actionSheet addButtonWithTitle:@"違反報告"];
    [actionSheet addButtonWithTitle:@"キャンセル"];
    actionSheet.cancelButtonIndex = 4;
    
    // アクションシートの表示
    [actionSheet showInView:self.view.superview];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString* title = [webView_ stringByEvaluatingJavaScriptFromString:@"document.title"];
    NSString* urlString = [webView_ stringByEvaluatingJavaScriptFromString:@"document.URL"];
    
    switch (buttonIndex) {
            // twitter
        case 0:{
            SLComposeViewController* slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [slComposeViewController setInitialText:title];
            [slComposeViewController addURL:[NSURL URLWithString:urlString]];
            [slComposeViewController addImage:[UIImage imageNamed:@"icon_120"]];
            [self presentViewController:slComposeViewController animated:YES completion:^{
                
            }];
        }
            
            break;
        // facebook
        case 1:{
            SLComposeViewController* slComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [slComposeViewController setInitialText:title];
            [slComposeViewController addURL:[NSURL URLWithString:urlString]];
            [slComposeViewController addImage:[UIImage imageNamed:@"icon_120"]];
            [self presentViewController:slComposeViewController animated:YES completion:^{
                
            }];
        }
            
            break;
        // line
        case 2:{
            NSString* lineText = [NSString stringWithFormat:@"%@ \n%@", title, urlString];
            NSString *contentType = @"text";
            NSString *contentKey = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                NULL,
                                                                                                (CFStringRef)lineText,
                                                                                                NULL,
                                                                                                (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                kCFStringEncodingUTF8 );
            NSString *urlString2 = [NSString
                                    stringWithFormat:@"line://msg/%@/%@",
                                    contentType, contentKey];
            NSURL *url = [NSURL URLWithString:urlString2];
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
        // 違反報告
        case 3:{
            NSString* path = [NSString stringWithFormat:@"ihan.php?url=%@", urlString];
            VknUrlLoadCommand* c = [[VknUrlLoadCommand alloc] initWithUrl:[Api url:path]];
            c.onComplete = ^(NSData* data){
                [self showToast:@"ご報告ありがとうございます"];
            };
            [c execute:nil];
            }
            break;
            
        default:
            break;
    }
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
    
}

-(void) tapBack{
    [self back];
}

-(void) swipeRight{
    [self back];
}

-(void) back{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) dealloc{
    [webView_ removeFromSuperview];
    webView_.delegate = nil;
    webView_ = nil;
}

-(void) showHeader{
    [UIView animateWithDuration:0.2f animations:^{
        header.transform = CGAffineTransformMakeTranslation(0, 0);
        webView_.frame = CGRectMake(0, 40, 320, [VknDeviceUtil windowHeight]-64-40);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideHeader{
    [UIView animateWithDuration:0.2f animations:^{
        header.transform = CGAffineTransformMakeTranslation(0, -40);
        webView_.frame = CGRectMake(0, 0, 320, [VknDeviceUtil windowHeight]-64);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) showMenu{
    [UIView animateWithDuration:0.2f animations:^{
        menuView.transform = CGAffineTransformMakeTranslation(0, 0);
        adView.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideMenu{
    [UIView animateWithDuration:0.2f animations:^{
        menuView.transform = CGAffineTransformMakeTranslation(0, 50);
        adView.transform = CGAffineTransformMakeTranslation(0, 50);
    } completion:^(BOOL finished) {
        
    }];
}

-(void) noHorizonScroll{
    CGSize contentSize = webView_.scrollView.contentSize;
    contentSize.width = 320;
    webView_.scrollView.contentSize = contentSize;
}

#pragma mark -------- UIWebViewDelegate -----------

-(void) webViewDidFinishLoad:(UIWebView *)webView{
//    if(!firstLoad) return;
//    firstLoad = NO;

    [self noHorizonScroll];
    [self hideLoading];
    
    webBackButton.enabled = [webView canGoBack];
    webNextButton.enabled = [webView canGoForward];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked ){
        [self showLoading];
    }
    
    return YES;
}

#pragma mark -------- UIScrollViewDelegate -----------

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self noHorizonScroll];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    float move = y - currntY;
    float sa = scrollView.contentSize.height - scrollView.frame.size.height - y;
    if(sa < 0) return;
    
    if(move > 0 && y > 0){
//        [self hideHeader];
        [self hideMenu];
    }

    if(move < 0){
//        [self showHeader];
        [self showMenu];
    }
    
    currntY = y;
}

#pragma mark -------- UIWebViewDelegate -----------

#pragma mark -------- public -----------

-(void) reload:(Article*)article_{
    firstLoad = YES;
    article = article_;
    titleLabel.text = article.title;
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:article.linkUrl]]];
    
    [self showLoading];
}

@end
