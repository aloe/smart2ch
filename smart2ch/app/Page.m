//
//  Page.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Page.h"
#import "ArticleListViewController.h"
#import "ArticleWebViewController.h"
#import "AdViewController.h"

@interface Page ()<
ArticleListViewDelegate
, ArticleWebViewDelegate
, AdViewDelegate
, UINavigationControllerDelegate
>{
    __weak NSObject<PageDelegate>* delegate;
    int index;
    UIView* view;
    UINavigationController* navigationController;
    ArticleListViewController* articleListViewController;
}

@end

@implementation Page

-(id) initWithDelegate:(NSObject<PageDelegate>*)delegate_ index:(int)index_{
    self = [super init];
    if(self){
        index = index_;
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    view = [[UIView alloc] initWithFrame:CGRectMake(index*320, 0, 320, [VknDeviceUtil windowHeight]-60)];
    articleListViewController = [[ArticleListViewController alloc] initWithDelegate:self];
    navigationController = [[UINavigationController alloc] initWithRootViewController:articleListViewController];
    navigationController.delegate = self;
    [navigationController setNavigationBarHidden:YES];
    [view addSubview:navigationController.view];
    
//    if(index == 1){
        [articleListViewController ad];
//    }
}

#pragma mark -------- UINavigationControllerDelegate -----------

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if([navigationController.viewControllers count] == 1){
        [delegate onHideWebView];
        [SMUtil dispatchGlobalEvent:APPLIPROMOTION_ICON_SHOW];
    }else{
        [delegate onShowWebView];
    }
}

#pragma mark -------- ArticleListViewDelegate -----------

-(void) onSelectArticle:(Article *)article{
    ArticleWebViewController* webViewController = [[ArticleWebViewController alloc] initWithDelegate:self];
    [navigationController pushViewController:webViewController animated:YES];
    [SMUtil dispatchGlobalEvent:APPLIPROMOTION_ICON_HIDE];
    [VknThreadUtil mainBlock:^{
        [webViewController reload:article];
    }];
}

-(void) onSelectAdArticle:(AdArticle *)adArticle{
    NSLog(@"onSelect:%@", adArticle.title);
    AdViewController* adViewController = [[AdViewController alloc] initWithDelegate:self];
    [navigationController pushViewController:adViewController animated:YES];
    [SMUtil dispatchGlobalEvent:APPLIPROMOTION_ICON_HIDE];
    [VknThreadUtil mainBlock:^{
        [adViewController reload:adArticle];
    }];
}

#pragma mark -------- public -----------

-(UIView*)getView{
    return view;
}

-(void) reload:(Site *)site{
    [articleListViewController reload:site];
}

-(void) reloadStatic:(Site *)site{
    [articleListViewController reloadStatic:site];
}

-(void) toRootViewcontroller{
    [navigationController popToRootViewControllerAnimated:NO];
}

@end
