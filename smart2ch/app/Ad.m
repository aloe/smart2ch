//
//  Ad.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Ad.h"
#import "ChkControllerDelegate.h"
#import "ChkController.h"
#import "AMoAdSDK.h"
#import <PAKit/PopAd.h>
#import "AdstirNativeAd.h"

static ChkController* chkController;
static AdArticleList* adcropsArticleList;
static AdArticleList* appliPromotionArticleList;

@interface ChkDelegate:NSObject<ChkControllerDelegate>

@end

@implementation ChkDelegate

- (void) chkControllerDataListWithSuccess:(NSDictionary *)data{
    NSLog(@"chkControllerDataListWithSuccess");
    if([chkController hasNextData]){
        [VknThreadUtil threadBlock:^{
            [chkController requestDataList];
        }];
//        return;
    }else{
        NSLog(@"endAdcropsData");
    }
    
    NSArray* dataList = chkController.dataList;
    if([dataList count] == 0) return;
    adcropsArticleList = [[AdArticleList alloc] initWithChkDatalist:dataList];
}

- (void) chkControllerDataListWithError:(NSError*)error{
    
}

- (void) chkControllerDataListWithNotFound:(NSDictionary *)data{

}

@end

@implementation Ad

static ChkDelegate* chkDelegate;

static UIViewController* rootViewController;

+ (Ad*)getInstance {
    static Ad* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[Ad alloc]
                           initSharedInstance];
    });
    return sharedSingleton;
}

- (id)initSharedInstance {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

-(void) setup:(UIViewController*)rootViewController_{
    rootViewController = rootViewController_;
    adcropsArticleList = [AdArticleList createDummy];
    
    // applipromotion
    [AMoAdSDK initSDK];
    [AMoAdSDK sendUUID];
    
    // POPAD
    [POPAD setMediaID:POPAD_MEDIA_ID];
    
    POPAD.log = YES;
    [POPAD setGlobalRefreshBlock:^(BOOL success, NSError *error) {
        if(error != nil){
            NSLog(@"------------fail------------");
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        NSLog(@"onRefresh");
        NSArray *array = POPAD.objects;
        NSLog(@"count:%d", [array count]);
        for(PAObject* obj in array){
            NSLog(@"name:%@", obj.title);
        }
        
    }];
}

-(void) reload{
    adcropsArticleList = [AdArticleList createDummy];
    
    chkDelegate = [[ChkDelegate alloc] init];
    chkController = [[ChkController alloc] initWithDelegate:chkDelegate];
    
    [POPAD refresh];
//    [POPAD action:POPAD_TAG_ARTICLELIST];
    
    // adcrops
//    [VknThreadUtil threadBlock:^{
        [chkController requestDataList];
//    }];
    
    // popad
//    [POPAD refresh];
    
    // applipromotion
//    [AMoAdSDK getAdsWithCount:100 completionBlock:^(NSInteger sts, NSArray *ads) {
//        NSLog(@"loadedApplipromotion:%d", [ads count]);
//        if(sts != 0 || ads.count == 0){
//            return;
//        }
//        
//        appliPromotionArticleList = [[AdArticleList alloc] initWithAppAdList:ads];
//    }];
}

-(void) showApplipromotion{
    [AMoAdSDK showAppliPromotionWall:rootViewController
                         orientation:UIDeviceOrientationPortrait
                     wallDrawSetting:APSDK_Ad_Key_WallDrawSetting_belowStatusBar
                              appKey:APPLIPROMOTION_APP_ID
                    onWallCloseBlock:^{
                        
                    }];
}

-(AdArticle*)adArticle{
//    if([appliPromotionArticleList count] == 0) return nil;
//    
//    int r = [VknUtils randInt:[appliPromotionArticleList count]];
//    return [appliPromotionArticleList get:r];
    
    if([adcropsArticleList count] == 0) return nil;
    
    int r = [VknUtils randInt:[adcropsArticleList count]];
    return [adcropsArticleList get:r];
}

-(void) showPopadInitial{
    NSLog(@"isSowPopad:%d", [SMUtil isShowPopad]);
    if(![SMUtil isShowPopad]) return;
    
    [VknUtils wait:2.0f callback:^{
        [POPAD action:POPAD_TAG_INITIAL];
    }];
}

-(void) showPopadBacktolist{
    NSLog(@"isSowPopad:%d", [SMUtil isShowPopad]);
    if(![SMUtil isShowPopad]) return;
    [POPAD action:POPAD_TAG_BACKTOLIST];
}

@end
