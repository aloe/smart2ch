//
//  AdArticleCell.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdArticle.h"

@protocol AdArticleDelegate <NSObject>

-(void) onSelectAdArticle:(AdArticle*)adArticle;

@end

@interface AdArticleCell : UITableViewCell

-(void) reload:(AdArticle*)adArticle_ index:(int)index;
-(void) setDelegate:(NSObject<AdArticleDelegate>*)delegate_;

@end
