//
//  ArticleBox.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleBox.h"

@interface ArticleBox (){
    __weak NSObject<ArticleBoxDelegate>* delegate;
    UIImageView* imageView;
    UILabel* titleLabel;
    Article* article;
    UIActivityIndicatorView* indicator;
    UIImageView* shadowImage;
    UIView* colorBg;
    UIView* overlay;
}

@end

@implementation ArticleBox

-(id) initWithDelegate:(NSObject<ArticleBoxDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    self.frame = CGRectMake(0, 0, 160, 160);
    UIView* bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    bgView.backgroundColor = UIColorFromHex(0xffffff);
    [self addSubview:bgView];
    
    CGRect imageFrame = CGRectMake(3, 3, 154, 154);
    colorBg = [[UIView alloc] initWithFrame:imageFrame];
    [self addSubview:colorBg];
    
    imageView = [[UIImageView alloc] initWithFrame:imageFrame];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [self addSubview:imageView];
    
    shadowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shadow"]];
    shadowImage.frame = CGRectMake(0, 154-50, 160, 50);
    [imageView addSubview:shadowImage];
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = imageView.frame;
    [self addSubview:indicator];
    
    titleLabel = [[UILabel alloc] init];
    [self addSubview:titleLabel];
    
    overlay = [[UIView alloc] initWithFrame:imageView.frame];
    overlay.backgroundColor = UIColorFromHex(0xaaaaaa);
    overlay.alpha = 0;
    [self addSubview:overlay];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBox)]];
}

-(void) tapBox{
    NSLog(@"tapBox");
    [delegate onSelectArticle:article];
    overlay.alpha = 0.5f;
    [UIView animateWithDuration:0.4f animations:^{
        overlay.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) right{
    self.frame = CGRectMake(160, 0, 160, 160);
}

-(void) reload:(Article*)article_ index:(int)index{
    article = article_;
    imageView.image = nil;
//    colorBg.hidden = YES;
    shadowImage.hidden = YES;
    colorBg.frame = CGRectMake(3, 3, 154, 154);
//    colorBg.backgroundColor = article.bgColor;
    float hue = (float)(index % 50) / 50.0;
//    colorBg.backgroundColor = [UIColor colorWithHue:hue saturation:0.5f brightness:0.8f alpha:1];
    colorBg.backgroundColor = UIColorFromHex(0xf7f7f7);
//    colorBg.backgroundColor = UIColorFromHex(0xf7f7f7);
    [indicator stopAnimating];
    
    if(![article hasImage]){
//        colorBg.hidden = NO;
//        colorBg.backgroundColor = article.bgColor;
        titleLabel.font = [UIFont systemFontOfSize:16.0f];
        titleLabel.frame = CGRectMake(10, 10, 140, 80);
        titleLabel.text = article.title;
        titleLabel.numberOfLines = 4;
        titleLabel.textColor = UIColorFromHex(0x252525);
        [titleLabel sizeToFit];
        titleLabel.layer.shadowColor = [UIColor clearColor].CGColor;
        return;
    }
    
    titleLabel.frame = CGRectMake(10, 113, 140, 40);
    titleLabel.text = article.title;
    titleLabel.numberOfLines = 2;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    titleLabel.layer.shadowOffset = CGSizeMake(1, 1);
    titleLabel.layer.shadowOpacity = 0.4f;
    titleLabel.layer.shadowRadius = 1;
    
    [indicator startAnimating];
    imageView.transform = CGAffineTransformMakeTranslation(0, 10);
    imageView.alpha = 0;
    [VknImageCache loadImage:article.imageUrl defaultImage:nil callback:^(UIImage *image, NSString *key, BOOL useCache) {
        if(![key isEqualToString:article.imageUrl]) return;
        [VknThreadUtil mainBlock:^{
            [indicator stopAnimating];
            imageView.image = image;
            shadowImage.hidden = NO;
            
            if(useCache){
                imageView.transform = CGAffineTransformMakeTranslation(0, 0);
                imageView.alpha = 1.0;
                return;
            }
            [UIView animateWithDuration:0.2f animations:^{
                imageView.transform = CGAffineTransformMakeTranslation(0, 0);
                imageView.alpha = 1.0;
            }];
        }];
    }];
}

-(void) mini{
    colorBg.frame = CGRectMake(3, 3, 154, 94);
}

@end
