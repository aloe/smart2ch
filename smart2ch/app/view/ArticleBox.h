//
//  ArticleBox.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ArticleBoxDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleBox : UIView

-(id) initWithDelegate:(NSObject<ArticleBoxDelegate>*)delegate_;
-(void) right;
-(void) reload:(Article*)article_ index:(int)index;
-(void) mini;

@end
