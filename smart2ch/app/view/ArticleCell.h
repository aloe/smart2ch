//
//  ArticleCell.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ArticleCellDelegate <NSObject>

-(void) onSelectArticle:(Article*)article;

@end

@interface ArticleCell : UITableViewCell

-(void) setDelegate:(NSObject<ArticleCellDelegate>*)delegate_;
-(void) reload:(Article*)leftArticle rightArticle:(Article*)rightArticle index:(int)index;
-(void) mini;

@end
