//
//  AdgameCell.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/16.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdgameCellDelegate <NSObject>

-(void) onSelectAdgame;

@end

@interface AdgameCell : UITableViewCell

-(void) setDelegate:(NSObject<AdgameCellDelegate>*)delegate_;
-(void) reload:(int)index;

@end
