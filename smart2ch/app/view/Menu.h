//
//  Menu.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuDelegate <NSObject>

-(void) onSelectSite:(int)siteIndex;

@end

@interface Menu : NSObject

-(id) initWithDelegate:(NSObject<MenuDelegate>*)delegate_;

-(UIView*)getView;
-(void) reload;
-(void) active:(int)index;
-(int) currentIndex;

@end
