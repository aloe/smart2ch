//
//  AdgameCell.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/16.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "AdgameCell.h"

@interface AdgameCell (){
    __weak NSObject<AdgameCellDelegate>* delegate;
    
    UIView* bgView;
    UIImageView* imageView;
    UILabel* titleLabel;
    UILabel* contentLabel;
    UIView* overlay;
}

@end

@implementation AdgameCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) setDelegate:(NSObject<AdgameCellDelegate>*)delegate_{
    delegate = delegate_;
}

-(void) initialize{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView* view = self.contentView;
    bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 3, 314, 94)];
    [view addSubview:bgView];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 74, 74)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.layer.cornerRadius = 15;
    [view addSubview:imageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 13, 207, 20)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    [view addSubview:titleLabel];
    
    contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(97, 33, 207, 54)];
    contentLabel.numberOfLines = 3;
    contentLabel.font = [UIFont systemFontOfSize:14.0f];
    [view addSubview:contentLabel];
    
    overlay = [[UIView alloc] initWithFrame:bgView.frame];
    overlay.backgroundColor = UIColorFromHex(0xaaaaaa);
    overlay.alpha = 0;
    [view addSubview:overlay];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
    
    
    
    // reload
    titleLabel.text = @"【PR】本日の無料ゲーム";
    contentLabel.text = @"2万人以上が毎日見てる！\n最強暇つぶしゲーム続々追加中";
    imageView.image = [UIImage imageNamed:@"todayGame"];
}

-(void) tap{
    overlay.alpha = 0.5f;
    [UIView animateWithDuration:0.4f animations:^{
        overlay.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
    }];
    
    [delegate onSelectAdgame];
}

-(void) reload:(int)index{
    float hue = (float)(index % 50) / 50.0;
    bgView.backgroundColor = [UIColor colorWithHue:hue saturation:0.5f brightness:0.8f alpha:1];
    
}

@end
