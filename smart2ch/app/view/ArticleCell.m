//
//  ArticleCell.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "ArticleCell.h"
#import "ArticleBox.h"

@interface ArticleCell ()<
ArticleBoxDelegate
>{
    __weak NSObject<ArticleCellDelegate>* delegate;
    ArticleBox* leftBox;
    ArticleBox* rightBox;
    UIView* line2;
}

@end

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    leftBox = [[ArticleBox alloc] initWithDelegate:self];
    [self.contentView addSubview:leftBox];
    
    rightBox = [[ArticleBox alloc] initWithDelegate:self];
    [rightBox right];
    [self.contentView addSubview:rightBox];
    
    UIView* line1 = [[UIView alloc] initWithFrame:CGRectMake(160, 0, 0.5f, 160)];
    line1.backgroundColor = UIColorFromHex(0xaaaaaa);
//    [self.contentView addSubview:line1];
    
    line2 = [[UIView alloc] init];
    line2.backgroundColor = UIColorFromHex(0xaaaaaa);
//    [self.contentView addSubview:line2];
    
    self.clipsToBounds = YES;
}

-(void) setDelegate:(NSObject<ArticleCellDelegate> *)delegate_{
    delegate = delegate_;
}

-(void) reload:(Article *)leftArticle rightArticle:(Article *)rightArticle index:(int)index{
    line2.frame = CGRectMake(0, 159.5f, 320, 0.5f);
    [leftBox reload:leftArticle index:index];
    [rightBox reload:rightArticle index:index];
}

-(void) mini{
    [leftBox mini];
    [rightBox mini];
    line2.frame = CGRectMake(0, 99.5f, 320, 0.5f);
}


#pragma mark -------- ArticleBoxDelegate -----------

-(void) onSelectArticle:(Article *)article{
    [delegate onSelectArticle:article];
}

@end
