//
//  Menu.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "Menu.h"


static UIView* line;

@interface MenuButton : UIView {
    Site* site;
    UIView* bgView;
    UIColor* bgColor;
    UIColor* textColor;
    UILabel* titleLabel;
}

-(void) active;
-(void) deactive;

@end

static float MENU_BUTTON_WIDTH = 80;
static float MENU_BUTTON_HEIGHT = 44;
@implementation MenuButton

-(id) initWithSite:(Site*)site_ index:(int)index{
    self = [super init];
    if(self){
        site = site_;
        [self initialize:index];
    }
    
    return self;
}

-(void) initialize:(int)index{
    CGRect frame = CGRectMake(MENU_BUTTON_WIDTH*index, 0, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
    self.frame = frame;
    
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT)];
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:bgView.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    bgView.layer.mask = maskLayer;
    
    [self addSubview:bgView];
    
    float fue = (float)index / (float)[[[Model getInstance] siteList] count];
    bgColor = [UIColor colorWithHue:fue saturation:0.6f brightness:0.9f alpha:1];
    bgColor = UIColorFromHex(0x00aff0);
    textColor = [UIColor colorWithHue:fue saturation:0.6f brightness:0.3f alpha:1];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(3, 0, MENU_BUTTON_WIDTH-6, MENU_BUTTON_HEIGHT)];
    titleLabel.textColor = UIColorFromHex(0xcccccc);
    titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    if(index == 0){
        titleLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    }
    titleLabel.numberOfLines = 2;
    [self addSubview:titleLabel];
    
    titleLabel.text = site.name;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = CGRectMake(3, 0, MENU_BUTTON_WIDTH-6, MENU_BUTTON_HEIGHT);
}

-(void) active{
//    bgView.backgroundColor = UIColorFromHex(0xcccccc);
    titleLabel.textColor = UIColorFromHex(0xffffff);
    line.backgroundColor = UIColorFromHex(0x00aff0);
    bgView.backgroundColor = UIColorFromHex(0x00aff0);
}

-(void) deactive{
    bgView.backgroundColor = UIColorFromHex(0xffffff);
    titleLabel.textColor = UIColorFromHex(0x707070);
}

@end

@interface Menu (){
    __weak NSObject<MenuDelegate>* delegate;
    UIView* view;
    UIScrollView* sv;
    NSArray* buttonList;
    int currentIndex;
}

@end

@implementation Menu

-(id) initWithDelegate:(NSObject<MenuDelegate>*)delegate_{
    self = [super init];
    if(self){
        delegate = delegate_;
        [self initialize];
    }
    
    return self;
}

-(void) initialize{
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, MENU_BUTTON_HEIGHT+20)];
    
    sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 20, 320, MENU_BUTTON_HEIGHT)];
    sv.contentInset = UIEdgeInsetsMake(0, 10, 0, 0);
    sv.contentOffset = CGPointMake(-10, 0);
    sv.showsHorizontalScrollIndicator = NO;
    [view addSubview:sv];
    
    line = [[UIView alloc] initWithFrame:CGRectMake(0, 20+MENU_BUTTON_HEIGHT-3, 320, 3)];
    line.backgroundColor = UIColorFromHex(0xc1181b);
    [view addSubview:line];
}

-(void) tapButton:(UITapGestureRecognizer*)reco{
    UIView* view = reco.view;
    int siteIndex = view.tag;
    
    [delegate onSelectSite:siteIndex];
}

#pragma mark -------- public ----------- 

-(UIView*)getView{
    return view;
}

-(void) reload{
    for(UIView* v in buttonList){
        [v removeFromSuperview];
    }
    
    SiteList* siteList = [[Model getInstance] siteList];
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0,max=[siteList count];i<max;i++){
        Site* site = [siteList get:i];
        MenuButton* b = [[MenuButton alloc] initWithSite:site index:i];
        b.tag = i;
        [b addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapButton:)]];
        [tmp addObject:b];
        [sv addSubview:b];
    }
    buttonList = [NSArray arrayWithArray:tmp];
    
    [self active:currentIndex];
    
    sv.contentSize = CGSizeMake(MENU_BUTTON_WIDTH*[siteList count] + 3, MENU_BUTTON_HEIGHT);
}

-(void) active:(int)index{
    currentIndex = index;
    for(MenuButton* button in buttonList){
        [button deactive];
    }
    
    MenuButton* targetButton = (MenuButton*) buttonList[index];
    [targetButton active];
    
    int buttonNum = [buttonList count];
    
    float toX = -3;
    if(index < 2){
        // 左端
        toX = -3;
    }else if(index >= buttonNum-2){
        // 右端
        toX = (buttonNum * MENU_BUTTON_WIDTH) - 320 + 3;
    }else{
        // 間
        float sa = index - 2;
        toX = (sa * MENU_BUTTON_WIDTH) + 40.0f;
    }
    
    [sv setContentOffset:CGPointMake(toX, 0) animated:YES];
}

-(int) currentIndex{
    return currentIndex;
}

@end
