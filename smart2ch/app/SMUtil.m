//
//  SMUtil.m
//  smart2ch
//
//  Created by kawase yu on 2014/08/10.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import "SMUtil.h"
#import "Ad.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface SMUtil ()

+(void) noReview;

@end

@interface AlertDelegate : NSObject<UIAlertViewDelegate>

@end


static BOOL isJaDevice;

@implementation AlertDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        // レビューする
        [VknUtils openBrowser:APPSTORE_URL];
        [SMUtil noReview];
    }else if(buttonIndex == 1){
        // あとで
        return;
    }else if(buttonIndex == 2){
        // レビューしない
        [SMUtil noReview];
    }
}

@end

@implementation SMUtil

static UIViewController* rootViewController;

+(UIViewController*)rootViewController{
    return rootViewController;
}

+(void) setup:(UIViewController*)rootViewController_{
    rootViewController = rootViewController_;
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString* countryCode = carrier.mobileCountryCode;
    isJaDevice = ([countryCode isEqualToString:@"440"] || [countryCode isEqualToString:@"441"]);
}

+(BOOL) isJaDevice{
    return isJaDevice;
}

static NSString* showedCountKey = @"showedCountKey";
static AlertDelegate* alertDelegate ;
+(void) tryShowRecommend{
    NSLog(@"tryShowRecommend");
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    int current = [ud integerForKey:showedCountKey];
    current++;
    [ud setInteger:current forKey:showedCountKey];
    [ud synchronize];
    
    if(!(current == 5 || current == 15 || current == 40 || current == 100)){
        [[Ad getInstance] showPopadInitial];
        return;
    }
    
    if([self isNoReview]) return;
    
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"ご意見お待ちしております";
    alertView.message = @"いつもご利用ありがとうございます。\nまだまだ貧弱なアプリですが、随時アップデートしてい予定です。\nこんな機能欲しい！\nここが使いにくい！\n等ありましたら、ぜひレビューをお願いしますm(^_^)m。";
    [alertView addButtonWithTitle:@"レビューする"];
    [alertView addButtonWithTitle:@"あとで"];
    [alertView addButtonWithTitle:@"レビューしない"];
    
    alertDelegate = [[AlertDelegate alloc] init];
    alertView.delegate = alertDelegate;
    
    [alertView show];
}

+(int) appActiveCount{
    return [[NSUserDefaults standardUserDefaults] integerForKey:showedCountKey];
}

+(BOOL) isShowPopad{
    NSLog(@"appActiveCount:%d", [self appActiveCount]);
    int current = [self appActiveCount];
    return (current > 5 && current %2 == 0);
}

static NSString* noReviewKey = @"noReviewKey";
+(BOOL) isNoReview{
    return [[NSUserDefaults standardUserDefaults] boolForKey:noReviewKey];
}

+(void) noReview{
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:noReviewKey];
    [ud synchronize];
}

+(void) addGlobalEventlistener:(id)target selector:(SEL)selector name:(NSString*)name{
    NSNotificationCenter* c = [NSNotificationCenter defaultCenter];
    [c addObserver:target selector:selector name:name object:nil];
}

+(void) dispatchGlobalEvent:(NSString*)name{
    NSNotification *n = [NSNotification notificationWithName:name object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:n];
}

@end
