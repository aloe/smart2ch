//
//  Ad.h
//  smart2ch
//
//  Created by kawase yu on 2014/08/11.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdArticleList.h"

@interface Ad : NSObject

+(Ad*)getInstance;
-(void) setup:(UIViewController*)rootViewController_;
-(void) reload;
-(AdArticle*)adArticle;
-(void) showApplipromotion;
-(void) showPopadInitial;
-(void) showPopadBacktolist;

@end
