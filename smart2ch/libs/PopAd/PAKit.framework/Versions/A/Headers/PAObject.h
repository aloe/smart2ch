//
//  PAObject.h
//  PAKit
//
//  Created by Terasawa Kazunori on 2014/02/10.
//  Copyright (c) 2014年 Nagisa,inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopAd.h"

@interface PAObject : NSObject

/** ID */
@property (nonatomic,readonly) NSString *objectID;


/*****************************************/
#pragma mark - 表示内容
/*****************************************/

/** 表示タイトル */
@property (nonatomic,readonly) NSString *title;
/** 表示本文 */
@property (nonatomic,readonly) NSString *body;
/** ジャンプボタンタイトル */
@property (nonatomic,readonly) NSString *buttonSubmit;
/** キャンセルボタンタイトル */
@property (nonatomic,readonly) NSString *buttonCancel;
/** リンク先URL */
@property (nonatomic,readonly) NSString *linkURL;

/*****************************************/
#pragma mark - 表示フィルタリング
/*****************************************/

/** アクションタグ */
@property (nonatomic,readonly) NSString *actionTag;
/** 最低バージョン */
@property (nonatomic,readonly) NSString *versionMin;
/** 最高バージョン */
@property (nonatomic,readonly) NSString *versionMax;
/** 表示タイプ */
@property (nonatomic,readonly) PADisplayType displayType;
/** 表示タイプ文字列 */
@property (nonatomic,readonly) NSString* displayTypeString;
/** 表示除外URLスキーム */
@property (nonatomic,readonly) NSString *excludeAppIdentifier;

/**
 * フィルタリング処理実行時にセットされる
 * 該当理由によりフィルタリングされた場合にフラグが立つ
 * None = 0 であれば通過
 */
@property (nonatomic,assign) NSInteger filterFlag;


/*****************************************/
#pragma mark - 表示間隔
/*****************************************/

/** 表示間隔 */
@property (nonatomic,readonly) NSInteger interval;
/** 初回スキップ */
@property (nonatomic,readonly) BOOL skipZero;

/*****************************************/
#pragma mark - 表示後
/*****************************************/

/** 自動的にSafariに飛ばす */
@property (nonatomic,readonly) BOOL autoLink;

/*****************************************/
#pragma mark - 内部管理パラメータ
/*****************************************/

/** displayTypeによる除外 */
@property (nonatomic,readonly) BOOL freeze;
/** アクションの対象となった回数(Interval管理用) */
@property (nonatomic,readonly) NSInteger actionCount;



@end
