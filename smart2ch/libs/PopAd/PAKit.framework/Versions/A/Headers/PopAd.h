//                                        __
/*************************************** /\ \ **********
 *     _____     ___   _____      __     \_\ \         *
 *    /\ '__`\  / __`\/\ '__`\  /'__`\   /'_` \        *
 *    \ \ \L\ \/\ \L\ \ \ \L\ \/\ \L\.\_/\ \L\ \       *
 *     \ \ ,__/\ \____/\ \ ,__/\ \__/.\_\ \___,_\      *
 *      \ \ \/  \/___/  \ \ \/  \/__/\/_/\/__,_ /      *
 *       \ \_\           \ \_\                         *
 *        \/_/            \/_/          version 1.0.0  *
 *                                                     *
 *******************************************************
 *                                                     *
 * PopAd.h                                             *
 *                                                     *
 * Copyright (c) 2013 Nagisa,inc. All rights reserved. *
 *                                                     *
 *******************************************************/


#ifndef __POPAD__
#define __POPAD__

@class PACore;
@class PAError;
@class PAObject;
@protocol PADelegate;

#define POPAD [PACore sharedCore]

/*****************************************/
#pragma mark - Notification Keys
/*****************************************/

/** 広告データ取得完了 */
extern NSString * const PADidRefreshNotification;
/** 広告データ表示完了 */
extern NSString * const PADidShowNotification;
/** 広告データ表示せず */
extern NSString * const PADidThroughNotification;
/** 広告データ表示に対してユーザのアクションが行われた */
extern NSString * const PADidTapNotification;


/*****************************************/
#pragma mark - Blocks
/*****************************************/

typedef void (^PARefreshBlock)(BOOL success,NSError* error);
typedef BOOL (^PAShouldShowBlock)(PAObject* obj);
typedef void (^PAShowBlock)(PAObject* obj);
typedef void (^PAActionBlock)(PAObject* obj, BOOL isSubmit);
typedef void (^PAThroughBlock)();

/*****************************************/
#pragma mark - UserAction
/*****************************************/

typedef NS_ENUM(NSInteger, PAUserAction){
    /** URL付きautolink=YES のボタン押下 */
    PAUserActionJump   = 1,
    /** URL付きautolink=NO のボタン押下 */
    PAUserActionSubmit = 2,
    /** URL無しボタン押下 */
    PAUserActionCancel = 3,
};

/*****************************************/
#pragma mark - DisplayType
/*****************************************/

typedef NS_ENUM(NSInteger, PADisplayType){
    /** 一度だけ */
    PADisplayTypeOnce,
    /** ジャンプした時のみ = URL付きのボタンを押したら以降表示されない */
    PADisplayTypeJump,
    /** 常に表示する */
    PADisplayTypeAll,
};

#if DEBUG == 1
#define PALog(fmt, ...) NSLog((@"%s L%d " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define PALog(fmt, ...)
#endif


#import "PACore.h"
#import "PADelegate.h"
#import "PAObject.h"

#endif
