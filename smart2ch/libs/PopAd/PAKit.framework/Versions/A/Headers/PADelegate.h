//
//  PADelegate.h
//  PAKit
//
//  Created by Terasawa Kazunori on 2013/11/25.
//  Copyright (c) 2013年 Nagisa,inc. All rights reserved.
//

#ifndef __POPAD_DELEGATE__
#define __POPAD_DELEGATE__

#import "PAObject.h"
#import "PopAd.h"

@class PAObject;

@protocol PADelegate <NSObject>
@optional
/* データの更新完了時*/
-(void)PARefreshCompleted:(BOOL)success error:(NSError*)error;

/* ポップアップの表示確認 */
-(BOOL)PAPopupShouldShow:(PAObject*)item;
/* ポップアップ表示完了後 */
-(void)PAPopupDidShow:(PAObject*)item;
/* ポップアップに表示する項目無し*/
-(void)PAPopupDidThrough;
/* ポップアップの各ボタンが表示された場合 */
-(void)PAPopupDidTap:(PAObject*)item isSubmit:(BOOL)isSubmit;

@end

#endif
