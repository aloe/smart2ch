//
//  PACore.h
//  PAKit
//
//  Created by Terasawa Kazunori on 2013/11/25.
//  Copyright (c) 2013年 Nagisa,inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PopAd.h"

@interface PACore : NSObject

/*****************************************/
#pragma mark - Access
/*****************************************/
/** スポットID */
@property (readonly) NSString *spotID;
/** 取得済み広告枠一覧 */
@property (readonly) NSArray  *objects;

/*****************************************/
#pragma mark - Blocks
/*****************************************/
/** Blocks Model (全体でブロックモデルを利用する場合) */

/** 更新完了 */
-(void)setGlobalRefreshBlock:(PARefreshBlock)globalRefreshBlock;
/** 表示リクエスト */
-(void)setGlobalShouldShowBlock:(PAShouldShowBlock)globalShouldShowBlock;
/** 表示直後 */
-(void)setGlobalShowBlock:(PAShowBlock)globalShowedBlock;
/** 表示せず */
-(void)setGlobalThroughBlock:(PAThroughBlock)globalThroughBlock;
/** ユーザがボタンを押した直後 */
-(void)setGlobalActionBlock:(PAActionBlock)globalActionBlock;


/*****************************************/
#pragma mark - Delegate
/*****************************************/

/** Delegate Model (デリゲートモデルを利用する場合) */
@property (nonatomic,assign) id<PADelegate> delegate;

/*****************************************/
#pragma mark - Lifecycle
/*****************************************/
/**
 *  コアオブジェクト
 *
 *  @return シングルトンオブジェクト
 */
+(PACore*)sharedCore;

/**
 *  初期化
 *  アプリケーション起動時にセットしてください
*/
@property (nonatomic,assign) NSString* mediaID;

@property (nonatomic,assign) BOOL log;

/*****************************************/
#pragma mark - Refresh
/*****************************************/
/**
 *  サーバから広告の一覧を取得
 *  データをロード/リフレッシュしたいときに呼んでください
 */
-(void)refresh;
/**
 *  @param completion 
 *  完了時に呼び出し 
 */
-(void)refreshWithCompletion:(PARefreshBlock)completion;

/*****************************************/
#pragma mark - Action
/*****************************************/

/**
 *  表示リクエストを実行
 *
 *  @param actions <NSString> 適用アクションタグ一覧
 *  @param shouldShow 表示許可を尋ねる
 *  @param show 表示時に呼び出し
 *  @param completion ボタン押下時に呼び出し
 */
-(void)action:(NSString *)action;
-(void)action:(NSString *)action didTap:(PAActionBlock)didTap;
-(void)action:(NSString *)action didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;
-(void)action:(NSString *)action didShow:(PAShowBlock)didShow didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;
-(void)action:(NSString *)action shouldShow:(PAShouldShowBlock)shouldShow didShow:(PAShowBlock)didShow didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;

-(void)actions:(NSArray *)actions;
-(void)actions:(NSArray *)actions didTap:(PAActionBlock)didTap;
-(void)actions:(NSArray *)actions didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;
-(void)actions:(NSArray *)actions didShow:(PAShowBlock)didShow didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;
-(void)actions:(NSArray *)actions shouldShow:(PAShouldShowBlock)shouldShow didShow:(PAShowBlock)didShow didTap:(PAActionBlock)didTap didThrough:(PAThroughBlock)didThrough;


@end
