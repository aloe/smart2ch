//
//  AdlantisInterstitialAd.h
//  AdLantis iOS SDK
//
//  Copyright 2014 Atlantis. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@protocol AdlantisInterstitialAdDelegate;
@class AdlantisTargetingParameters;

@interface AdlantisInterstitialAd : NSObject

+ (instancetype)interstitialAdWithPublisherId:(NSString*)publisherId;

- (void)show;

- (void)cancel;

@property (nonatomic,weak) id<AdlantisInterstitialAdDelegate> delegate;

@property (nonatomic,strong) AdlantisTargetingParameters *targetingParameters;

@end


@protocol AdlantisInterstitialAdDelegate <NSObject>

@optional

- (void)interstitialAdRequestComplete:(AdlantisInterstitialAd*)interstitialAd;

- (void)interstitialAdRequestFailed:(AdlantisInterstitialAd*)interstitialAd;

- (void)interstitialAdWillBePresented:(AdlantisInterstitialAd*)interstitialAd;

- (void)interstitialAdWasDismissed:(AdlantisInterstitialAd*)interstitialAd;

@end
