//
//  AdlantisView.h
//  AdLantis iOS SDK
//
//  Copyright 2009-2014 Atlantiss.jp. All rights reserved.
//

#import <UIKit/UIKit.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
typedef NS_ENUM(NSUInteger, AdlantisViewTransition) {
  AdlantisViewTransitionFadeIn,
  AdlantisViewTransitionSlideFromRight,
  AdlantisViewTransitionSlideFromLeft,
  AdlantisViewTransitionNone
};

@protocol AdlantisViewDelegate;
@class AdlantisTargetingParameters;

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface AdlantisView : UIView

@property (nonatomic,strong)
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
IBInspectable
#endif
NSString *publisherID;

@property (nonatomic,weak) id<AdlantisViewDelegate> delegate;

@property (nonatomic,assign) AdlantisViewTransition defaultTransition;

// amount of time (in seconds) before fetching next set of ads, set to zero to stop ad fetch
@property (nonatomic,assign) NSTimeInterval adFetchInterval;

// amount of time (in seconds) before showing the next ad
@property (nonatomic,assign) NSTimeInterval adDisplayInterval;

@property (nonatomic,weak) UIViewController *rootViewController;

@property (nonatomic,strong) AdlantisTargetingParameters *targetingParameters;

- (IBAction)showNextAd:(id)sender;
- (IBAction)showPreviousAd:(id)sender;

@property (nonatomic,readonly) BOOL collapsed;
- (IBAction)collapse;
- (IBAction)uncollapse;
- (IBAction)toggleCollapse;

- (IBAction)fadeIn;
- (IBAction)fadeOut;
- (IBAction)toggleFaded;

- (void)requestAds;

@property (nonatomic,readonly) UIInterfaceOrientation aspectOrientation;     // the orientation for which ads are shown

+ (CGSize)sizeForOrientation:(UIInterfaceOrientation)orientation;

@end

///////////////////////////////////////////////////////////////////////////////////////////////////

@protocol AdlantisViewDelegate <NSObject>

- (void)bannerAdRequestComplete:(AdlantisView*)adView;

- (void)bannerAdRequestFailed:(AdlantisView*)adView;

@optional

// The bannerAdPreview methods are only used for mediation when the mediated ad shows a preview.
- (void)bannerAdPreviewWillBeShown:(AdlantisView*)adView;

- (void)bannerAdPreviewWillBeHidden:(AdlantisView*)adView;

- (void)bannerAdTouched:(AdlantisView*)adView;

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
typedef NS_ENUM(NSUInteger, AdlantisViewLocation) {
  AdlantisViewLocationAtTop = 0,
  AdlantisViewLocationAtBottom,
  AdlantisViewLocationElsewhere,
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// standard view sizes for orientation
#ifdef __cplusplus
extern "C" {
#endif
  
  CGSize AdlantisViewSizeForOrientation(UIInterfaceOrientation orientation);
  AdlantisViewLocation AdlantisLocationForView(UIView *view);
  
#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
// send when ads are updated
extern NSString * const AdlantisAdsUpdatedNotification                  DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol");
extern NSString * const AdlantisAdsUpdatedNotificationAdCount           DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol"); // NSNumber(int) count of ads received
extern NSString * const AdlantisAdsUpdatedNotificationCached            DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol"); // NSValue(bool) were the ads loaded from the cache?
extern NSString * const AdlantisAdsUpdatedNotificationError             DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol"); // NSValue(bool) was there an error?
extern NSString * const AdlantisAdsUpdatedNotificationNSError           DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol"); // NSError error that occurred (not always available)
extern NSString * const AdlantisAdsUpdatedNotificationErrorDescription  DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol"); // NSString describing error (not always available)

///////////////////////////////////////////////////////////////////////////////////////////////////
extern NSString * const AdlantisViewAdTouchedNotification               DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol");

///////////////////////////////////////////////////////////////////////////////////////////////////
extern NSString * const AdlantisPreviewWillBeShownNotification          DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol");
extern NSString * const AdlantisPreviewWillBeHiddenNotification         DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView AdlantisViewDelegate delegate protocol");

