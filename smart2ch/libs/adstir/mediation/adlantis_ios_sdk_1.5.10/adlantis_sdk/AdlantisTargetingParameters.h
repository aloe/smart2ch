//
//  AdlantisTargetingParameters.h
//  AdLantis iOS SDK
//
//  Created on 15/08/2014.
//  Copyright (c) 2014 Atlantis Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@interface AdlantisTargetingParameters : NSObject

@property (nonatomic) CLLocationCoordinate2D location;

@end
