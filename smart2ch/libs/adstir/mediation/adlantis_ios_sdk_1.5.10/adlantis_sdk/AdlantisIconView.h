//
//  AdlantisIconView.h
//  AdLantis iOS SDK
//
//  Copyright 2014 Atlantiss.jp. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@class AdlantisIconAdSet;

extern const NSInteger kAdlantisIconWidth;
extern const NSInteger kAdlantisIconHeight;
extern const NSInteger kAdlantisTextIconHeight;

@interface AdlantisIconView : UIView

@property (weak, nonatomic,readonly) AdlantisIconAdSet *set;
@property (strong, nonatomic)
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
IBInspectable
#endif
UIColor *textColor;

@end
