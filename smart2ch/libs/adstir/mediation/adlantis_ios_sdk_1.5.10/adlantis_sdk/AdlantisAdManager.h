//
//  AdlantisAdManager.h
//  AdLantis iOS SDK
//
//  Copyright 2009-2014 Atlantiss.jp. All rights reserved.
//

#import <Foundation/Foundation.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface AdlantisAdManager : NSObject

+ (instancetype)sharedManager;

+ (NSString*)versionString;                                       // AdLantis SDK version
+ (NSString*)build;                                               // AdLantis SDK build
@property (nonatomic,readonly,copy) NSString *fullVersionString;  // Complete version information in one line

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface AdlantisAdManager(deprecated)

@property (nonatomic,readonly,copy) NSString *byline              DEPRECATED_ATTRIBUTE;

@property (nonatomic,copy) NSString *host                         DEPRECATED_ATTRIBUTE;

@property (nonatomic,strong) NSString *isoCountryCode             DEPRECATED_ATTRIBUTE;

@property (nonatomic,strong) NSString *publisherID                DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView publisherID method");

@property (nonatomic,assign) NSTimeInterval adFetchInterval       DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView adFetchInterval method");
@property (nonatomic,assign) NSTimeInterval adDisplayInterval     DEPRECATED_MSG_ATTRIBUTE("use the AdlantisView adDisplayInterval method");

@property (nonatomic,copy) NSArray *testAdRequestURLs             DEPRECATED_ATTRIBUTE;

// deprecated - use the classes in AdLantisConversion.h
- (void)sendConversionTag:(NSString*)inConversionTag              DEPRECATED_MSG_ATTRIBUTE("use the AdLantisConversion class");
- (void)sendConversionTagTest:(NSString*)inConversionTag          DEPRECATED_MSG_ATTRIBUTE("use the AdLantisConversion class");

@end
