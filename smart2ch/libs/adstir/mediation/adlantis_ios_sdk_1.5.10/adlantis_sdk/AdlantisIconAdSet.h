//
//  AdlantisIconAdSet.h
//  AdLantis iOS SDK
//
//  Copyright 2014 Atlantiss.jp. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@class AdlantisIconView;
@class AdlantisTargetingParameters;
@protocol AdlantisIconAdSetDelegate;

@interface AdlantisIconAdSet : NSObject

+ (instancetype)iconAdSetWithPublisherId:(NSString*)publisherId;

- (void)add:(AdlantisIconView*)iconView;

- (void)remove:(AdlantisIconView*)iconView;

- (void)load;

@property (nonatomic,readonly) NSUInteger count;

@property (nonatomic,strong,readonly) NSString *publisherId;

@property (nonatomic,weak) id<AdlantisIconAdSetDelegate> delegate;

@property (nonatomic,strong) AdlantisTargetingParameters *targetingParameters;

@end


@protocol AdlantisIconAdSetDelegate <NSObject>

@optional

- (void)iconAdRequestComplete:(AdlantisIconAdSet*)iconSet count:(NSUInteger)count;

- (void)iconAdRequestFailed:(AdlantisIconAdSet*)iconSet;

@end


@interface AdlantisIconAdSet(deprecated)

+ (instancetype)iconSetWithPublisherId:(NSString*)publisherId              __attribute__((deprecated("use the iconAdSetWithPublisherId: method")));

@end
