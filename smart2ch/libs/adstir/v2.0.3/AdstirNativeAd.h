//
//  Copyright (c) 2014 UNITED, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AdstirNativeAd;


@interface AdstirNativeAdResponse : NSObject

@property (nonatomic, readonly, copy) NSString* iconUrl;
@property (nonatomic, readonly, copy) NSString* imageUrl;
@property (nonatomic, readonly, copy) NSString* cta;
@property (nonatomic, readonly, copy) NSString* landingPageUrl;
@property (nonatomic, readonly, copy) NSString* title;
@property (nonatomic, readonly, copy) NSString* description;
@property (nonatomic, readonly, assign) float rating;
- (void)impression;
- (void)click;

+ (void)bindImageWithURL:(NSURL *)url bindCallback:(void (^) (UIImage *image))callback;

- (void)bindImageToImageView:(UIImageView *)imageView;
- (void)bindIconToImageView:(UIImageView *)imageView;

- (void)bindImageToButton:(UIButton *)button;
- (void)bindIconToButton:(UIButton *)button;

@end


@protocol AdstirNativeAdDelegate <NSObject>

// Performed when receive ad. / 広告が取得できた際に呼び出されます。
- (void)adstirNativeAdDidReceiveAd:(AdstirNativeAd*)nativeAd response:(AdstirNativeAdResponse*)response;
// Performed when fail to receive ad. / 広告が取得できなかった際に呼び出されます。
- (void)adstirNativeAdDidFailToReceiveAd:(AdstirNativeAd*)nativeAd;

@end


@interface AdstirNativeAd : NSObject
@property (nonatomic, weak) id<AdstirNativeAdDelegate> delegate;
// Set media ID. / メディアIDを設定します。
@property (nonatomic, copy) NSString *media;
// Set spot number. / 枠Noを設定します。
@property (nonatomic, assign) NSUInteger spot;

@property (nonatomic, copy) NSString *sponsoredText;
@property (nonatomic, assign) NSUInteger titleLength;
@property (nonatomic, assign) NSUInteger descriptionLength;
@property (nonatomic, assign) NSUInteger ctaLength;
@property (nonatomic, assign) BOOL image;
@property (nonatomic, assign) BOOL icon;
@property (nonatomic, assign) BOOL rating;
@property (nonatomic, assign) BOOL landingPageUrl;
- (void)getAd;
- (id)init;

@end



